(function($) {
    /*---- Login Ajax ----*/
   $("form#login-form").submit(function (e){
       e.preventDefault();
       let url = $(this).data('url');
       let formData = new FormData(this);
       console.log(url)

       $.ajax({
           url: url,
           type: "post",
           data: formData,

           beforeSend: function (){
               $(".overlay-snipper").addClass("open");
           },

           success: function (data){
               console.log(data)
                if(data.code === 200){
                    window.location = "http://website_shop/admin/user";
                }
           },

           error: function (error){
               let e = error.responseJSON;
               console.log(e)
               if(e.code === 500){
                   alert(e.message);
               }
           },

           complete: function (){
               $(".overlay-snipper").removeClass("open");
           },

           contentType: false,
           cache: false,
           processData: false,

       });
   })
})(jQuery);
