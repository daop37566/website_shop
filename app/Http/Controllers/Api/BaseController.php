<?php


namespace App\Http\Controllers\Api;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class BaseController extends Controller
{

    protected $age = ['3-6', '6-12', '13-16', '16-18', '19-25', '25-30', '30-40', '40-'];
    protected $gender = ['nam', 'nữ', 'unknown'];
    const ERROR = 'Error.';
    const VALIDATION_ERROR = 'Validation Error.';
    const NUMERIC = 'required|numeric';
    const DATE = 'required|date';
    const TIME = 'required|date_format:H:i:s';
    const DAY = 'required|numeric|min:1|max:31';

    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendResponse($result, $message)
    {
        $response = [
            'success' => true,
            'data'    => $result,
            'message' => $message,
        ];


        return response()->json($response, 200);
    }


    /**
     * return error response.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendError($error, $errorMessages = [], $code = 404)
    {
        $response = [
            'success' => false,
            'message' => $error,
        ];


        if(!empty($errorMessages)){
            $response['data'] = $errorMessages;
        }


        return response()->json($response, $code);
    }

    public function upload($file)
    {
        $filenameOrigin = $file->getClientOriginalName();
        $filenameHash = Str::random(5) . '.' . $file->getClientOriginalExtension();
        $path = $file->storeAs('public/images', $filenameHash);

        $dataUpload = [
            'file_name' => $filenameOrigin,
            'file_path' => Storage::url($path)
        ];
        return $dataUpload["file_path"];
    }
}