<div class="shop-sidebar mb-30">
    <h4 class="title">Filter By Price</h4>
    <!-- filter-price-content start -->
    <div class="filter-price-content">
        <form>
            <div id="price-slider" class="price-slider"></div>
            <div class="filter-price-wapper">
                <input type="submit" class="add-to-cart-button" value="FILTER">
                <div class="filter-price-cont">
                    <span>Price:</span>
                    <div class="d-flex">
                        <input type="text" id="amount" readonly style="border: none; outline: none">
                        <input type="hidden" name="min-price" id="min-price">
                        <input type="hidden" name="max-price" id="max-price">
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- filter-price-content end -->
</div>
