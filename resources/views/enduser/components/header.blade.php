@php
    use Harimayco\Menu\Facades\Menu;
    use Harimayco\Menu\Models\MenuItems;
    $menus = MenuItems::all();
@endphp

<header>
    <!-- mobile menu -->
    <div class="mobile-menu  bg-second">
        <a href="#" class="mb-logo">Ruiz</a>
        <span class="mb-menu-toggle" id="mb-menu-toggle">
            <i class='bx bx-menu'></i>
        </span>
    </div>
    <!-- end mobile menu -->
    <!-- main header -->
    <div class="header-wrapper" id="header-wrapper">
        <span class="mb-menu-toggle mb-menu-close" id="mb-menu-close">
            <i class='bx bx-x'></i>
        </span>
        <!-- top header -->
        <div class="bg-second">
            <div class="top-header container">
                <ul class="devided">
                    <li>
                        <a href="#">+84397552977</a>
                    </li>
                    <li>
                        <a href="#">daop37566@mail.com</a>
                    </li>
                </ul>
                <ul class="devided">
                    <li class="dropdown">
                        <a href="">USD</a>
                        <i class='bx bxs-chevron-down'></i>
                        <ul class="dropdown-content">
                            <li><a href="#">VND</a></li>
                            <li><a href="#">JPY</a></li>
                            <li><a href="#">EUR</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="">ENGLISH</a>
                        <i class='bx bxs-chevron-down'></i>
                        <ul class="dropdown-content">
                            <li><a href="#">VIETNAMESE</a></li>
                            <li><a href="#">JAPANESE</a></li>
                            <li><a href="#">FRENCH</a></li>
                            <li><a href="#">SPANISH</a></li>
                        </ul>
                    </li>
                    @if(!\Illuminate\Support\Facades\Auth::guard("customer")->check())
                        <li><a href="{{ route("auth.login") }}">Đăng nhập</a></li>
                    @else
                        <li><a href="{{ route("auth.logout") }}">Đăng xuất</a></li>
                    @endif
                </ul>
            </div>
        </div>
        <!-- end top header -->
        <!-- mid header -->
         <!-- mid header -->
         <div class="haeader-mid-area bg-gren border-bm-1 d-none d-lg-block ">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-3 col-md-4 col-5">
                        <div class="logo-area">
                            <a href="{{ route("page.index") }}">
                                <img src="{{ asset("picture/logo.png") }}" alt=""></a>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="search-box-wrapper">
                            <div class="search-box-inner-wrap">
                                <form class="search-box-inner" method="GET" action="{{ route("shop.searchBrand") }}">
                                    <div class="search-field-wrap">
                                        <input type="text" class="search-field search-brand" placeholder="Tìm kiếm thương hiệu..." name="keyword">

                                        <div class="search-btn">
                                            <button class="btn-confirm"><i class="icon-magnifier"></i></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="right-blok-box text-white d-flex">
                            <div class="user-wrap">
                                <a href="{{ route("wishList.showWishList") }}">
                                    <span class="cart-total">@if(!empty($wishlist)){{ count($wishlist) }} @else 0 @endif</span>
                                    <i class="icon-heart"></i>
                                </a>
                            </div>
                            <div class="shopping-cart-wrap">
                                <a href="#">
                                    <i class="icon-basket-loaded"></i>
                                    @if(Session::has('cart') != null && Session::has('cart') != '' )
                                            <span id="total-show" class="cart-total">{{Session::get("cart")->totalQuanty}}</span>
                                        @else
                                            <span id="total-show" class="cart-total">0</span>
                                        @endif

                                </a>
                                <ul class="mini-cart" id="cart-list">
                                    @if (Session::has('cart') != null)
                                    @foreach (Session::get('cart')->products as $item)
                                        <li class="cart-item">

                                            <div class="cart-image d-flex justify-content-center">
                                                <a href="">
                                                    <img width="50" alt="" src="{{  \App\Helper\Functions::getImage('product', $item['productInfo']->image1 ) }}">
                                                </a>
                                            </div>
                                            <div class="cart-title">
                                                <a href="">
                                                    <h4>{{$item['productInfo']->name}}</h4>
                                                </a>
                                                <div class="quanti-price-wrap">
                                                    <span class="quantity "> {{$item['quanty']}} x </span>
                                                    <div class="price-box"><span class="new-price">{{$item['productInfo']->price_final}}</span></div>
                                                </div>
                                            </div>

                                        </li>
                                    @endforeach

                                        <li class="subtotal-box">
                                        <div class="subtotal-title">
                                            <h3>Sub-Total :{{Session::get('cart')->totalPrice}}</h3>
                                            <span>$</span>
                                            <input type="hidden" id="total-cart"  value="{{(Session::has('cart') != null)  ? Session::get('cart')->totalQuanty : '0'}}"></input>
                                        </div>
                                        </li>
                                        <li class="mini-cart-btns">
                                        <div class="cart-btns">
                                            <a href="{{ route("cart.showCart") }}">Giỏ hàng</a>
                                            <a href="{{ route("checkout.checkLoginToCheckOut") }}">Thanh toán</a>
                                        </div>
                                        </li>
                                    @else
                                        <li class="cart-item p-0 justify-content-center">
                                            <span class="text-dark"> Giỏ hàng trống !!</span>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- end mid header -->
        <!-- bottom header -->
        <div class="bg-second">
            <div class="bottom-header container">
                <ul class="main-menu">
                    <!-- mega menu -->
                    @foreach($menus as $menu)
                        @if($menu -> parent != 0)
                            <li class="active"><a href="index.html">Home <i class="fa fa-angle-down"></i></a>
                                <ul class="sub-menu">
                                    <li><a href="index.html">Home Page 1</a></li>
                                    <li><a href="https://demo.hasthemes.com/ruiz-preview/ruiz/index-2.html">Home Page 2</a></li>
                                </ul>
                            </li>
                        @else
                            <li><a href="{{ $menu -> link }}"> {{ $menu -> label }}</a></li>
                        @endif
                    @endforeach
                </ul>
            </div>
        </div>
        <!-- end bottom header -->
    </div>
    <!-- end main header -->
</header>

<style>

</style>
