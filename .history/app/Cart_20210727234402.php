<?php
    namespace App;

    class Cart{
        public $products = null;
        public $totalPrice = 0;
        public $totalQuanty = 0;

        public function __construct($cart){
            if($cart){
                $this->products = $cart->products;
                $this->totalPrice = $cart->totalPrice;
                $this->totalQuanty = $cart->totalQuanty;
            }
        }
        public function AddCart($product,$id){
            $newProduct = ['quanty' => 0 ,'price' => $product->price_final,'productInfo' => $product];
            if($this->products){
                if(array_key_exists($id, $this->products)){
                    $newProduct = $this->products[$id];
                }
            }
            $newProduct['quanty']++;
            $newProduct['price'] = $newProduct['quanty'] * $product->price_final ;

            $this->products[$id] = $newProduct;
            $this->totalPrice += $product->price_final;
            $this->totalQuanty++;

        }

        public function AddCartDetail($id,$quanty, $product){
            if(isset($this->products[$id])){
                $quatity = $this->products[$id]['quanty'] += $quanty;
                $price = $this->products[$id]['price'] * $quatity;
                $this->totalPrice += $price;
                $this->totalQuanty += $quatity;

            }else{
                $this->AddCart($product,$id);
            }
        }

        public function DeleteItemCart($id){
            $this->totalQuanty -= $this->products[$id]['quanty'];
            $this->totalPrice -=  $this->products[$id]['price'];
            unset($this->products[$id]);
        }

        public function UpdateItemList($id ,$quanty){
            $this->totalQuanty -= $this->products[$id]['quanty'];
            $this->totalPrice -= $this->products[$id]['price'];

            $this->products[$id]['quanty'] = $quanty;
            $this->products[$id]['price'] = $quanty * $this->products[$id]['productInfo']->price_final  ;

            $this->totalQuanty += $this->products[$id]['quanty'];
            $this->totalPrice += $this->products[$id]['price'];
        }

    }
?>
