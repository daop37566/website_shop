<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    protected $pathView = "auth.core.";

    public function login(){
        return view($this -> pathView . "login");
    }

    public function authenticate(Request $request){
            $email = $request -> email;
            $password = $request -> password;
            $remember = $request -> remember_token ? true : false;

            if(Auth::attempt(['email' => $email, 'password' => $password], $remember)){
                return response() -> json([
                    'code' => 200,
                    'message' => 'Đăng nhập thành công',
                ], 200);
            }
    }
    public function logout(){
        Auth::logout();
        return redirect() -> route("authAdmin.login");
    }
}
