<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    protected $pathView = "auth.core.";

    public function login(){
        return view($this -> pathView . "login");
    }

    public function postLogin(Request $request){
        $email = $request->email;
        $password = $request->password;
        $remember = $request->remember_me ? true : false;
        Auth::attempt(['email' => $email, 'password' => $password], $remember);
        if (Auth::check()) {
            return redirect()->route('admin.user.index');
        } else {
            return redirect()->route('auth.login')->withErrors(trans('message.invalid_login_account'));
        }
    }
    public function logout(){
        Auth::logout();
        return redirect() -> route("authAdmin.login");
    }
}
