<?php

namespace App\Http\Controllers\EndUser;

use App\Coupon;
use App\Http\Controllers\Controller;
use App\Product;
use App\Cart;
use Illuminate\Http\Request;

class CartController extends Controller
{
    protected $pathView = "enduser.pages.Cart.";
    protected $pathViewCom = "enduser.components.";

    public function addToCart(Request $req,$id){
        $product = Product::where('id',$id)->first();
        if($product != null){
            $oldCart = Session('cart') ? Session('cart')  : null;
            $newCart = new Cart($oldCart);
            $newCart->AddCart($product,$id);

            $req->Session()->put('cart',$newCart);

        }
        return view($this->pathViewCom . "cart");
    }
    public function deleteProduct(Request $req,$id){

            $oldCart = Session('cart') ? Session('cart')  : null;
            $newCart = new Cart($oldCart);
            $newCart->DeleteItemCart($id);

            if(Count($newCart->products) > 0){
                $req->Session()->put('cart',$newCart);
            }else{
                $req->Session()->forget('cart',$newCart);
            }

        return view($this->pathView . "cart");
    }

    public function showCart(){
        return view($this->pathView . "cart");
    }

    public function DeleteListItemCart(Request $req,$id){


        $oldCart = Session('cart') ? Session('cart')  : null;
        $newCart = new Cart($oldCart);
        $newCart->DeleteItemCart($id);

        if(Count($newCart->products) > 0){
            $req->Session()->put('cart',$newCart);
        }else{
            $req->Session()->forget('cart',$newCart);
        }

        return view('front-end.page.list-cart');
    }
    public function updateCart(Request $req) {
        foreach($req -> data as $item ){
            $oldCart = Session('cart') ? Session('cart')  : null;
            dd($oldCart);
            $newCart = new Cart($oldCart);
            $newCart->UpdateItemList($item["key"] ,$item["value"]);
            $req->Session()->put('cart',$newCart);
        }
    }

    public function checkout(){
        return view($this -> pathView . "checkout");
    }

    public function applyCoupon(Request $request){
        $data['carts'] = session() -> get("cart");

        if(isset($request -> nameCoupon)){
            $data["coupon"] = Coupon::where('name', $request -> nameCoupon)->first();
            $cartsView = view("enduser.components.carts") -> with($data) -> render();
            return response() -> json([
                'data' => $cartsView,
                'code' => 200
            ],200);
        }
    }
}
