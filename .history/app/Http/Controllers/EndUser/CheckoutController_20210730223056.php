<?php

namespace App\Http\Controllers\EndUser;

use App\Banks;
use App\Coupon;
use App\District;
use App\Http\Controllers\Controller;
use App\Order;
use App\OrderAddress;
use App\OrderDetail;
use App\Province;
use App\Ward;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Session\Session;

class CheckoutController extends Controller
{
    protected $pathView = "enduser.pages.Checkout.";

    public function checkLoginToCheckOut(){
        if( !Auth::guard("customer")->check() ){
            return redirect() -> route("auth.login");
        }
        else{
            $data["wishlist"] = session() -> get("wishList");
            $data["carts"] = session()->get("cart");
            $data["provinces"] = Province::orderBy("_name", "ASC")->get();
            $data["districts"] = District::orderBy("_name", "ASC")->get();
            $data["wards"] = Ward::orderBy("_name", "ASC")->get();
            // dd($data);
            $data["banks"] = Banks::where("status", "active")->get();
            //dd($data["provinces"]);
            return view($this -> pathView . "checkout")->with($data);
        }
    }


    public function getDistrict($id){
        $district = District::where('_province_id' ,$id)->get();
        echo "<option value> Chon huyen/thanhpho  </option>";
        foreach($district as $item){
            echo "<option value=".$item->id.">$item->_name</option>" ;
        }
    }
    public function getWard($id){
        $ward = Ward::where('_district_id' ,$id)->get();
        echo "<option value> Chon xa/phuong  </option>";
        foreach($ward as $item){

            echo "<option value=".$item->id.">$item->_name</option>" ;
        }
    }
    public function confirmCheckout(Request $request){
        if(!empty($request['coupon_id'])){
            $coupon = Coupon::where("name", $request['coupon_id'])->first();
        }

        $user_id = Auth::user()->id;
        $data = $request -> all();
        //dd($request -> all());

        //Store data into order, order_detail table
        $order_address = new OrderAddress();
        $order_address -> user_id = $user_id;

        $order = new Order();
        $order -> user_id = $user_id;

        foreach ($data as $key => $value){
            if($key == "pay_method" || $key == "note" || $key == "price_total"){
                $order -> $key = $value;
            }
            else if($key == "bank"){
                unset($key);
            }
            else if($key == "coupon_id"){
                $order -> $key = @$coupon -> id;
            }
            else{
                if($key == "province"){
                    $province_name = Province::find($value);
                    $value = $province_name -> _name;
                }
                if($key == "district"){
                    $district_name = District::find($value);
                    $value = $district_name -> _name;
                }
                if($key == "ward"){
                    $ward_name = Ward::find($value);
                    $value = $ward_name -> _name;
                }
                $order_address -> $key = $value;
            }

        }

        $order_address -> save();

        //Save address id after store order_address into database
        $order -> address_id = $order_address -> id;
        $order -> save();

        if( Session::has('cart')){
            $carts = Session::get('cart')->products;
        }
        //Store data into order_detail table
        // foreach ($carts as $cart){
        //     $order_detail = new OrderDetail();
        //     $order_detail -> user_id = $user_id;
        //     $order_detail -> order_id = $order -> id;
        //     $order_detail -> product_id = $cart['productInfo']->id;
        //     $order_detail -> product_name = $cart['productInfo']->name;
        //     $order_detail -> product_picture = $cart['productInfo']->picture;
        //     $order_detail -> product_price = $cart['productInfo']->price_final;
        //     $order_detail -> product_quantity =  $cart['quanty'];
        //     $order_detail -> price_total = $cart['price'];
        //     $order_detail -> status = 'Đang chờ xử lý';
        //     $order_detail -> save();
        // }

        // return response() -> json([
        //     'code' => 200,
        //     // 'orderId' => $order -> id,
        // ],200);

    }

    public function applyCoupon(Request $request){
        $couponActive = Coupon::where("name", $request -> nameCoupon)->first();
        if($couponActive){
            return response() -> json([
                'code' => 200,
                'data' => $couponActive,
            ],200);
        }
        else{
            return response() -> json([
                'code' => 500,
            ]);
        }
    }

    public function checkoutSuccess(){
        session() -> forget("cart");
        $data['carts'] = session() -> get("cart");
        return view($this -> pathView ."doneCheckout")->with($data);
    }


}
