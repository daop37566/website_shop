<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    protected $pathView = "auth.core.";

    public function login(){
        return view($this -> pathView . "login");
    }

    public function postLogin(Request $request){
        return redirect()->route('admin.user.index');
    }
    public function logout(){
        Auth::logout();
        return redirect() -> route("authAdmin.login");
    }
}
