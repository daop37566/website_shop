<?php

namespace App\Http\Controllers\Api;

use App\Blproduct;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helper\Functions;
use App\Product;
use Illuminate\Support\Facades\Validator;
use PhpParser\Node\Expr\FuncCall;

class BlockchainController extends BaseController
{
    public function index()
    {
        $products =  Blproduct::get();

        return $this->sendResponse($products, 'Lấy danh sách Sản Phẩm thành công');
        // return ProductResource::collection($products);
    }

    public function store(Request $request){
        try {

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'owner' => 'required',
            ]);

            if ($validator->fails()) {
                return $this->sendError(self::VALIDATION_ERROR, $validator->errors());
            }

            $product = new Blproduct();
            $product->name = $request->name;
            $product->owner = $request->owner;
            $product->save();
            return $this->sendResponse([], 'Tạo tài khoản thành công');
        } catch (\Exception $e) {

            return $this->sendError(self::ERROR, $e->getMessage());
        }
    }


    public function edit(Request $request)
    {
        try {





            Blproduct::where('id', $request->id)->update([
                'purchase' => 'true',
            ]);



            return $this->sendResponse([], 'Mua thành công');
        } catch (\Exception $e) {

            return $this->sendError(self::ERROR, $e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
