<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helper\Functions;
use App\Http\Resources\ProductController as ProductResource;
use App\QA;
use PhpParser\Node\Expr\FuncCall;

class QaController extends BaseController
{
    public function index()
    {
        $qa =  QA::get();
        $data = [];
        foreach($qa as $key => $t) {
            foreach($t as $key1 => $t1) {
            if($key1 == 'correctAnswer'){
                $a =  [
                    'text' => $t1 ,
                    'correct' => true
                ];
                array_push($data, $a);
            }
        }


        return $this->sendResponse($data, 'Lấy danh sách Cau hoi tra loi thành công');
        // return ProductResource::collection($products);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
