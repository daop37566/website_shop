<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
use App\Helper\Functions;
use App\Http\Resources\ProductController as ProductResource;
use App\QA;
use PhpParser\Node\Expr\FuncCall;

class ProductController extends BaseController
{
    public function index()
    {
        $qa =  QA::get();


        return $this->sendResponse($qa, 'Lấy danh sách Cau hoi tra loi thành công');
        // return ProductResource::collection($products);
    }
    public function productDetail($slug){
        $product =  Product::where('slug', $slug
        )->first();
        $product -> image1 = Functions::getImage("product", $product -> image1);
                $product -> image2 = Functions::getImage("product", $product -> image2);
                $test =  json_decode($product->gallery,true);
                if (!empty($test)){
                $a = [];
                foreach($test as $t) {
                    $a[] = asset('picture/product/'.$t);
                }
                $product->gallery = json_encode($a);
                }
        return $this->sendResponse($product, 'Lấy danh sách Sản Phẩm thành công');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
