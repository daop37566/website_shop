<?php

namespace App\Http\Controllers\Api;

use App\Customer;
use Illuminate\Http\Request;
use App\Http\Resources\UserController as UserResource;
use App\Helper\Functions;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class UserController extends BaseController
{
    /**
     * Display the specified resource.
     * Not display password and remember_token because in User model was hidden these
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    // public function index()
    // {
    //     $users =  User::where('status', 'active')->get();

    //     foreach ($users as $key => $user){
    //         if($user -> picture){
    //             $user -> picture = Functions::getImage("user", $user -> picture);
    //         }
    //     }

    //     return UserResource::collection($users);
    // }

    // public function store(Request $request)
    // {
    //     if(DB::table('user')->insert($request->all())){
    //         return response()->json("User has been created",201);
    //     }
    //     else{
    //         return response()->json("Can not create");
    //     }
    // }

    // public function show($id)
    // {
    //     //
    // }

    // public function update(Request $request, $id)
    // {
    //     //
    // }

    // public function destroy($id)
    // {
    //     //
    // }
    public function postRegister(Request $request)
    {

        try {

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'required|email|unique:customers',
                'password' => 'required',
                'confirm_password' => 'required|same:password',
                'phone' => 'required',
            ]);

            if ($validator->fails()) {
                return $this->sendError(self::VALIDATION_ERROR, $validator->errors());
            }

            $user = new Customer();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->phone = $request->phone;
            $user->status = 'inactive';
            $user->save();
            return $this->sendResponse([], 'Tạo tài khoản thành công');
        } catch (\Exception $e) {

            return $this->sendError(self::ERROR, $e->getMessage());
        }
    }

    public function login(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email',
            'password' => 'required|string'
        ]);
        if ($validator->fails()) {
            return $this->sendError(self::VALIDATION_ERROR, $validator->errors());
        }
        $credentials = request(['email', 'password']);

        if (!Auth::guard("customer")->attempt($credentials)) {
            return $this->sendError('Unauthorized', [], 401);
        }

        $user = Auth::guard("customer")->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;

        if ($request->remember_me) {
            $token->expires_at = Carbon::now()->addWeeks(1);
        }

        $token->save();

        return $this->sendResponse([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ], 'Đăng nhập thành công');
    }
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return $this->sendResponse([], 'Đăng xuất thành công');
    }
}
