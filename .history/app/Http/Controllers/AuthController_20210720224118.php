<?php

namespace App\Http\Controllers;
use Session;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    protected $pathView = "auth.core.";

    public function login(){
        return view($this -> pathView . "login");
    }

    public function postLogin(Request $request){

        if($request->isMethod('post')){
            Validator::make($request->all(),[
                'email' => 'required|email|max:255',
                'password' => 'required|min:6|max:16',
            ],
                [
                'email.required' => 'Email người dùng không được để trống',
                'email.email' => 'Bạn phải đăng nhập bằng email',
                'password.required' => 'Bắt buộc phải nhập mật khẩu'
                ]
            )->validate();
            $remember = $request->remember_me ? true : false;
            $arr = [
                'email' => $request->email,
                'password' => $request->password
            ];
            if(Auth::attempt($arr,$remember)){
                Session::put('user', $request->email);
                return redirect()->route('admin.user.index');
            }else{
                return redirect()->route('authAdmin.login')->with('thongbao','Tài khoản or mật khẩu không chính xác');
            }
        }
        return redirect()->back();
    }
    public function logout(){
        Auth::logout();
        return redirect() -> route("authAdmin.login");
    }
}
