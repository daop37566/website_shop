<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    protected $pathView = "auth.core.";

    public function login(){
        return view($this -> pathView . "login");
    }

    public function authenticate(Request $request){
        try{
            $email = $request -> email;
            $password = $request -> password;
            $remember = $request -> remember_token ? true : false;
            dd(Auth::guest());
            if(Auth::attempt(['email' => $email, 'password' => $password], $remember)){
                if (Auth::check()) {
                    return redirect()->route('admin.user');
                } else {
                    return redirect()->route('auth.login')->withErrors(trans('message.invalid_login_account'));
                }
            }
        }
        catch (\Exception $e){
            $e -> getMessage();
        }
    }
    public function logout(){
        Auth::logout();
        return redirect() -> route("authAdmin.login");
    }
}
