<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
class Customer extends Authenticatable
{
    use Notifiable, HasApiTokens;

    protected $fillable = ["name", "picture", "phone", "email", "password","status"];

    protected $hidden = ["password"];
}
