<?php

return [
    'user' => [
        'index' => 'List User',
        'create' => 'Create User',
        'edit' => 'Edit User',
        'delete' => 'Delete User'
    ],

    'role' => [
        'index' => 'List Role',
        'create' => 'Create Role',
        'edit' => 'Edit Role',
        'delete' => 'Delete Role'
    ],

    'product' => [
        'index' => 'List Product',
        'create' => 'Create Product',
        'edit' => 'Edit Product',
        'delete' => 'Delete Product'
    ],

    'product_category' => [
        'index' => 'List Product Category',
        'create' => 'Create Product Category',
        'edit' => 'Edit Product Category',
        'delete' => 'Delete Product Category'
    ],

    'product_tags' => [
        'index' => 'List Product Tag',
        'create' => 'Create Product Tag',
        'edit' => 'Edit Product Tag',
        'delete' => 'Delete Product Tag'
    ],

    'setting' => [
        'index' => 'List Setting',
        'create' => 'Create Setting',
        'edit' => 'Edit Setting',
        'delete' => 'Delete Setting'
    ],

    'partner' => [
        'index' => 'List Partner',
        'create' => 'Create Partner',
        'edit' => 'Edit Partner',
        'delete' => 'Delete Partner'
    ],

    'menu' => [
        'index' => 'List Menu',
    ],

    'blog' => [
        'index' => 'List Blog',
        'create' => 'Create Blog',
        'edit' => 'Edit Blog',
        'delete' => 'Delete Blog'
    ],

    'blog_category' => [
        'index' => 'List Blog Category',
        'create' => 'Create Blog Category',
        'edit' => 'Edit Blog Category',
        'delete' => 'Delete Blog Category'
    ],

    'banner' => [
        'index' => 'List Banner',
        'create' => 'Create Banner',
        'edit' => 'Edit Banner',
        'delete' => 'Delete Banner'
    ],

    'coupon' => [
        'index' => 'List Coupon',
        'create' => 'Create Coupon',
        'edit' => 'Edit Coupon',
        'delete' => 'Delete Coupon'
    ],
    'customer' => [
        'index' => 'List Coupon',
        'create' => 'Create Coupon',
        'edit' => 'Edit Coupon',
        'delete' => 'Delete Coupon'
    ],

    'widget' => [
        'index' => 'List Widget',
        'create' => 'Create Widget',
        'edit' => 'Edit Widget',
        'delete' => 'Delete Widget'
    ],
    'bank' => [
        'index' => 'List Widget',
        'create' => 'Create Widget',
        'edit' => 'Edit Widget',
        'delete' => 'Delete Widget'
    ],
    'qa' => [
        'index' => 'List Widget',
        'create' => 'Create Widget',
        'edit' => 'Edit Widget',
        'delete' => 'Delete Widget'
    ],
];
