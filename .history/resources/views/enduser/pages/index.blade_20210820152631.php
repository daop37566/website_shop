@extends("enduser.layout")
@section("front_content")

        <!-- hero section -->
    <div class="hero">
        <div class="slider">
            <div class="container">
                <!-- slide item -->
                @foreach($banners as $key => $banner)
                <div class="slide">
                    <div class="info">
                        <div class="info-content">
                            <h3 class="top-down">
                                {{ $banner -> sale }}
                            </h3>
                            <h2 class="top-down trans-delay-0-2">
                                {{ $banner -> name }}
                            </h2>
                            <p class="top-down trans-delay-0-4">
                                {{ $banner -> description }}
                            </p>
                            <div class="top-down trans-delay-0-6">
                                <button class="btn-flat btn-hover">
                                    <span>shop now</span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="img left-right">
                        <img src="{{ \App\Helper\Functions::getImage('banner', $banner -> picture) }}" alt="">
                    </div>
                </div>
                @endforeach
                <!-- end slide item -->
            </div>
            <!-- slider controller -->
            <button class="slide-controll slide-next">
                <i class='bx bxs-chevron-right'></i>
            </button>
            <button class="slide-controll slide-prev">
                <i class='bx bxs-chevron-left'></i>
            </button>
            <!-- end slider controller -->
        </div>
    </div>
    <!-- end hero section -->
    <!-- promotion section -->
    <div class="promotion">
        <div class="row">
            <div class="col-4 col-lg-4 col-md-12 col-sm-12">
                <div class="promotion-box">
                    <div class="text">
                        <h3>Headphone & Earbuds</h3>
                        <button class="btn-flat btn-hover"><span>shop collection</span></button>
                    </div>
                    <img src="https://toppng.com/uploads/preview/headphone-11530972541wpwpfogtyu.png" alt="">
                </div>
            </div>
            <div class="col-4 col-lg-4 col-md-12 col-sm-12">
                <div class="promotion-box">
                    <div class="text">
                        <h3>JBL Quantum Series</h3>
                        <button class="btn-flat btn-hover"><span>shop collection</span></button>
                    </div>
                    <img src="https://salt.tikicdn.com/cache/w1200/ts/product/2b/96/d3/90d1856d2265fbe0cbfcca86bd51575e.JPG" alt="">
                </div>
            </div>
            <div class="col-4 col-lg-4 col-md-12 col-sm-12">
                <div class="promotion-box">
                    <div class="text">
                        <h3>True Wireless Earbuds</h3>
                        <button class="btn-flat btn-hover"><span>shop collection</span></button>
                    </div>
                    <img src="https://img.my-best.vn/press_component/item_part_images/f28b1796d70c239f14ab3ba7f5720c0e.jpg?ixlib=rails-4.2.0&q=70&lossless=0&w=640&h=640&fit=clip&s=b0f1f5a76406804fd684e6bc5483a0c4" alt="">
                </div>
            </div>
        </div>
    </div>
    <!-- end promotion section -->
    <!-- product list -->
    <div class="section">
        <div class="container">
            <div class="section-header">
                <h2>Latest product</h2>
            </div>
            <div class="row" >
                @foreach($best_seller as $product)
                    <div class="col-3 col-lg-3 col-md-6 col-sm-12">
                        <div class="product-card">
                            <div class="product-card-img">
                                    <img src="{{ \App\Helper\Functions::getImage("product", $product -> image1) }}" alt="">
                                    <img src="{{ \App\Helper\Functions::getImage("product", $product -> image2) }}" alt="">
                            </div>
                            <div class="product-card-info">
                                <div class="product-btn">
                                    <a href="{{ route("shop.productDetail", ['slug' => $product -> slug]) }}" class="btn-flat btn-hover btn-shop-now">shop now</a>
                                    <button class="btn-flat btn-hover btn-cart-add" onclick="AddCart({{$product->id}})">
                                        <i class='bx bxs-cart-add'></i>
                                    </button>
                                    <button class="btn-flat btn-hover btn-cart-add wishlist-btn" title="Add to Wish List" data-url="{{ route("wishList.addWishList", ["id" => $product->id]) }}">
                                        <i class='bx bxs-heart'></i>
                                    </button>
                                </div>
                                <div class="product-card-name">
                                    <a href="{{ route("shop.productDetail", ['slug' => $product -> slug]) }}">
                                        {{ $product -> name }}
                                    </a>
                                </div>
                                <div class="product-card-price">
                                    @if ($product->price_final == '' && $product->price_final == null)
                                    <span><del>${{ number_format($product -> price_base) }}</del></span>
                                    @else
                                    <span><del>${{ number_format($product -> price_base) }}</del></span>
                                    <span class="curr-price">${{ number_format($product -> price_final) }}</span>
                                    @endif

                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="section-footer">
                <a href="./products.html" class="btn-flat btn-hover">view all</a>
            </div>
        </div>
    <!-- end product list -->


@stop

<style>
    .top-down{
        margin: 10px 0;
        padding: 20px 0;
    }
</style>
