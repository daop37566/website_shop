@extends("enduser.layout")
@section("front_content")
    <div class="front-content">

        <!-- hero section -->
    <div class="hero">
        <div class="slider">
            <div class="container">
                <!-- slide item -->
                @foreach($slidebars as $key => $slidebar)
                <div class="slide">
                    <div class="info">
                        <div class="info-content">
                            <h3 class="top-down">
                                {{ $slidebar -> sale }}
                            </h3>
                            <h2 class="top-down trans-delay-0-2">
                                {{ $slidebar -> name }}
                            </h2>
                            <p class="top-down trans-delay-0-4">
                                {{ $slidebar -> description }}
                            </p>
                            <div class="top-down trans-delay-0-6">
                                <button class="btn-flat btn-hover">
                                    <span>shop now</span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="img left-right">
                        <img src="{{ \App\Helper\Functions::getImage('banner', $slidebar -> picture) }}" alt="">
                    </div>
                </div>
                @endforeach
                <!-- end slide item -->
            </div>
            <!-- slider controller -->
            <button class="slide-controll slide-next">
                <i class='bx bxs-chevron-right'></i>
            </button>
            <button class="slide-controll slide-prev">
                <i class='bx bxs-chevron-left'></i>
            </button>
            <!-- end slider controller -->
        </div>
    </div>
    <!-- end hero section -->
    <!-- promotion section -->
    <div class="promotion">
        <div class="row">
            <div class="col-4 col-md-12 col-sm-12">
                <div class="promotion-box">
                    <div class="text">
                        <h3>Headphone & Earbuds</h3>
                        <button class="btn-flat btn-hover"><span>shop collection</span></button>
                    </div>
                    <img src="./images/JBLHorizon_001_dvHAMaster.png" alt="">
                </div>
            </div>
            <div class="col-4 col-md-12 col-sm-12">
                <div class="promotion-box">
                    <div class="text">
                        <h3>JBL Quantum Series</h3>
                        <button class="btn-flat btn-hover"><span>shop collection</span></button>
                    </div>
                    <img src="./images/kisspng-beats-electronics-headphones-apple-beats-studio-red-headphones.png" alt="">
                </div>
            </div>
            <div class="col-4 col-md-12 col-sm-12">
                <div class="promotion-box">
                    <div class="text">
                        <h3>True Wireless Earbuds</h3>
                        <button class="btn-flat btn-hover"><span>shop collection</span></button>
                    </div>
                    <img src="./images/JBL_TUNE220TWS_ProductImage_Pink_ChargingCaseOpen.png" alt="">
                </div>
            </div>
        </div>
    </div>
    <!-- end promotion section -->

        <!-- Sản phẩm bán chạy -->
        <div class="product-area section-pb section-pt-30">
            <div class="container">

                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-title">
                            <h4>Sản phẩm bán chạy</h4>
                        </div>
                    </div>
                </div>

                <div class="row product-active-lg-4">
                    @foreach($best_seller as $product)
                        <div class="col-lg-12">
                            <!-- single-product-area start -->
                            <div class="single-product-area mt-30">
                                <div class="product-thumb">
                                    <a class="product-wrapper" href="{{ route("shop.productDetail", ['slug' => $product -> slug]) }}">
                                        <img class="primary-image" src="{{ \App\Helper\Functions::getImage("product", $product -> picture) }}" alt="">
                                    </a>
                                    @include("enduser.components.actions", ["id_cart" => $product -> id])
                                </div>
                                <div class="product-caption">
                                    <h4 class="product-name"><a href="{{ route("shop.productDetail", ['slug' => $product -> slug]) }}">{{ $product -> name }}</a></h4>
                                    <div class="price-box">
                                        @if($product -> price_final == $product -> price_base)
                                            <span class="new-price">${{ number_format($product -> price_final) }}</span>
                                        @else
                                            <span class="new-price">${{ number_format($product -> price_final) }}</span>
                                            <span class="old-price">${{ number_format($product -> price_base) }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <!-- single-product-area end -->
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <!-- Product Area End -->

        <!-- Banner Area Start -->
        <div class="banner-area">
            <div class="container">
                <div class="row">
                    @foreach($banners as $item)
                        <div class="col-lg-6 col-md-6">
                            <div class="single-banner mb-30">
                                <a href="{{ route("shop.showProductByBrand", ["slug" => $item->slug]) }}">
                                    <img
                                        style="border-radius: 5px;"
                                        src="{{ \App\Helper\Functions::getImage("banner", $item -> picture) }}" alt="">
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <!-- Banner Area End -->

        <!-- Sản phẩm mới -->
        <div class="product-area section-pb section-pt-30">
            <div class="container">

                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-title">
                            <h4>Sản phẩm sale</h4>
                        </div>
                    </div>
                </div>

                <div class="row product-active-lg-4">
                    @foreach($product_sale as $product)
                        <div class="col-lg-12">
                            <!-- single-product-area start -->
                            <div class="single-product-area mt-30">
                                <div class="product-thumb">
                                    <a class="product-wrapper" href="{{ route("shop.productDetail", ['slug' => $product -> slug]) }}">
                                        <img class="primary-image" src="{{ \App\Helper\Functions::getImage("product", $product -> picture) }}" alt="">
                                    </a>
                                    @include("enduser.components.actions", ["id_cart" => $product -> id])
                                </div>
                                <div class="product-caption">
                                    <h4 class="product-name"><a href="">{{ $product -> name }}</a></h4>
                                    <div class="price-box">
                                        @if($product -> price_final == $product -> price_base)
                                            <span class="new-price">${{ number_format($product -> price_final) }}</span>
                                        @else
                                            <span class="new-price">${{ number_format($product -> price_final) }}</span>
                                            <span class="old-price">${{ number_format($product -> price_base) }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <!-- single-product-area end -->
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <!-- Product Area End -->

        <!-- our-brand-area start -->
        <div class="our-brand-area section-pb">
            <div class="container">
                @include("enduser.components.partner", ["partners" => $partners])
            </div>
        </div>
        <!-- our-brand-area end -->

        <!-- letast blog area Start -->
        <div class="letast-blog-area section-pb">
            <div class="container">
                <div class="row">
                @foreach($blogs as $blog)
                    <div class="col-lg-4">
                        <div class="singel-latest-blog">
                            <div class="aritcles-content">
                                <div class="author-name">
                                    post by: <a href="#"> {{ @$blog -> author }}</a> - {{ date_format(@$blog-> updated_at, "d/m/Y H:i:s") }}
                                </div>
                                <h4>
                                    <a href="{{ route("blog.blogDetail", ["slug" => @$blog -> slug]) }}" class="articles-name">
                                        {{ @$blog -> name }}
                                    </a>
                                </h4>
                            </div>
                            <div class="articles-image">
                                <a href="{{ route("blog.blogDetail", ["slug" => @$blog -> slug]) }}">
                                    <img src="{{ \App\Helper\Functions::getImage("blog", @$blog -> picture, "thumbnail") }}" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
                </div>
            </div>
        </div>
        <!-- letast blog area End -->


        <!-- promotion section -->
        <div class="promotion">
            <div class="row">
                <div class="col-4 col-md-12 col-sm-12">
                    <div class="promotion-box">
                        <div class="text">
                            <h3>Headphone & Earbuds</h3>
                            <button class="btn-flat btn-hover"><span>shop collection</span></button>
                        </div>
                        <img src="./images/JBLHorizon_001_dvHAMaster.png" alt="">
                    </div>
                </div>
                <div class="col-4 col-md-12 col-sm-12">
                    <div class="promotion-box">
                        <div class="text">
                            <h3>JBL Quantum Series</h3>
                            <button class="btn-flat btn-hover"><span>shop collection</span></button>
                        </div>
                        <img src="./images/kisspng-beats-electronics-headphones-apple-beats-studio-red-headphones.png" alt="">
                    </div>
                </div>
                <div class="col-4 col-md-12 col-sm-12">
                    <div class="promotion-box">
                        <div class="text">
                            <h3>True Wireless Earbuds</h3>
                            <button class="btn-flat btn-hover"><span>shop collection</span></button>
                        </div>
                        <img src="./images/JBL_TUNE220TWS_ProductImage_Pink_ChargingCaseOpen.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    <!-- end promotion section -->

        <div class="newletter-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="newletter-wrap">
                            <div class="row align-items-center">
                                <div class="col-lg-7 col-md-12">
                                    <div class="newsletter-title mb-30">
                                        <h3>Join Our <br><span>Newsletter Now</span></h3>
                                    </div>
                                </div>

                                <div class="col-lg-5 col-md-7">
                                    <div class="newsletter-footer mb-30">
                                        <input type="text" placeholder="Your email address...">
                                        <div class="subscribe-button">
                                            <button class="subscribe-btn">Subscribe</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@stop

<style>
    .top-down{
        margin: 10px 0;
        padding: 20px 0;
    }
</style>
