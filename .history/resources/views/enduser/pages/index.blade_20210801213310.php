@extends("enduser.layout")
@section("front_content")

    <style>
        .container.grid, .container.columns {
  align-content: stretch;
  align-items: stretch;
  flex-wrap: wrap;
}

.letter {
  text-align: center;
  color: black;
  font-size: 10vmax;
  font-weight: 400;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 2px 6px;
}
.container.grid .letter {
  flex-basis: 50%;
}
.container.columns .letter {
  flex-basis: 25%;
}
.for, .gsap {
  font-size: 5vmax;
  color: white;
}
.for {
  padding: 2px 1.6vmax;
  font-weight: 300;
  display: none;
}
.gsap {
  padding: 2px 0;
  font-weight: 600;
  display: none;
}
.container.final .for, .container.final .gsap {
  display: block;
}
.F {
  background: rgba(0, 188, 212, 0.7);
}
.l {
  background: rgba(40, 150, 255, 0.7);
}
.i {
  background: rgba(153, 80, 220, 0.7);
}
.p {
  background: rgba(90, 108, 225, 0.7);
}
.container.plain .letter {
  background: transparent;
  color: white;
  padding: 0;
}

.logo {
  position: fixed;
  width: 60px;
  bottom: 20px;
  right: 30px;
}

    </style>
        <!-- hero section -->
    <div class="hero">
        <div class="slider">
            <div class="container">
                <div class="container final">
                    <div class="letter F">F</div>
                    <div class="letter l">l</div>
                    <div class="letter i">i</div>
                    <div class="letter p">p</div>
                    <div class="for">for</div>
                    <div class="gsap">GSAP</div>
                </div>
                <!-- slide item -->
                {{-- @foreach($slidebars as $key => $slidebar)
                <div class="slide">
                    <div class="info">
                        <div class="info-content">
                            <h3 class="top-down">
                                {{ $slidebar -> sale }}
                            </h3>
                            <h2 class="top-down trans-delay-0-2">
                                {{ $slidebar -> name }}
                            </h2>
                            <p class="top-down trans-delay-0-4">
                                {{ $slidebar -> description }}
                            </p>
                            <div class="top-down trans-delay-0-6">
                                <button class="btn-flat btn-hover">
                                    <span>shop now</span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="img left-right">
                        <img src="{{ \App\Helper\Functions::getImage('banner', $slidebar -> picture) }}" alt="">
                    </div>
                </div>
                @endforeach --}}
                <!-- end slide item -->
            </div>
            <!-- slider controller -->
            {{-- <button class="slide-controll slide-next">
                <i class='bx bxs-chevron-right'></i>
            </button>
            <button class="slide-controll slide-prev">
                <i class='bx bxs-chevron-left'></i>
            </button> --}}
            <!-- end slider controller -->
        </div>
    </div>
    <script>
        gsap.registerPlugin(Flip);

let layouts = ["final", "plain", "columns", "grid"],
		container = document.querySelector(".container"),
		curLayout = 0; // index of the current layout

function nextState() {
  const state = Flip.getState(".letter, .for, .gsap", {props: "color,backgroundColor", simple: true}); // capture current state

  container.classList.remove(layouts[curLayout]); // remove old class
  curLayout = (curLayout + 1) % layouts.length;   // increment (loop back to the start if at the end)
  container.classList.add(layouts[curLayout]);    // add the new class

  Flip.from(state, { // animate from the previous state
    absolute: true,
    stagger: 0.07,
    duration: 0.7,
    ease: "power2.inOut",
    spin: curLayout === 0, // only spin when going to the "final" layout
    simple: true,
    onEnter: (elements, animation) => gsap.fromTo(elements, {opacity: 0}, {opacity: 1, delay: animation.duration() - 0.1}),
    onLeave: elements => gsap.to(elements, {opacity: 0})
  });

  gsap.delayedCall(curLayout === 0 ? 3.5 : 1.5, nextState);
}

gsap.delayedCall(1, nextState);
    </script>
    <!-- end hero section -->
    <!-- promotion section -->
    <div class="promotion">
        <div class="row">
            <div class="col-4 col-lg-4 col-md-12 col-sm-12">
                <div class="promotion-box">
                    <div class="text">
                        <h3>Headphone & Earbuds</h3>
                        <button class="btn-flat btn-hover"><span>shop collection</span></button>
                    </div>
                    <img src="https://toppng.com/uploads/preview/headphone-11530972541wpwpfogtyu.png" alt="">
                </div>
            </div>
            <div class="col-4 col-lg-4 col-md-12 col-sm-12">
                <div class="promotion-box">
                    <div class="text">
                        <h3>JBL Quantum Series</h3>
                        <button class="btn-flat btn-hover"><span>shop collection</span></button>
                    </div>
                    <img src="https://salt.tikicdn.com/cache/w1200/ts/product/2b/96/d3/90d1856d2265fbe0cbfcca86bd51575e.JPG" alt="">
                </div>
            </div>
            <div class="col-4 col-lg-4 col-md-12 col-sm-12">
                <div class="promotion-box">
                    <div class="text">
                        <h3>True Wireless Earbuds</h3>
                        <button class="btn-flat btn-hover"><span>shop collection</span></button>
                    </div>
                    <img src="https://img.my-best.vn/press_component/item_part_images/f28b1796d70c239f14ab3ba7f5720c0e.jpg?ixlib=rails-4.2.0&q=70&lossless=0&w=640&h=640&fit=clip&s=b0f1f5a76406804fd684e6bc5483a0c4" alt="">
                </div>
            </div>
        </div>
    </div>
    <!-- end promotion section -->
    <!-- product list -->
    <div class="section">
        <div class="container">
            <div class="section-header">
                <h2>Latest product</h2>
            </div>
            <div class="row" >
                @foreach($best_seller as $product)
                    <div class="col-3 col-lg-3 col-md-6 col-sm-12">
                        <div class="product-card">
                            <div class="product-card-img">
                                    <img src="{{ \App\Helper\Functions::getImage("product", $product -> image1) }}" alt="">
                                    <img src="{{ \App\Helper\Functions::getImage("product", $product -> image2) }}" alt="">
                            </div>
                            <div class="product-card-info">
                                <div class="product-btn">
                                    <a href="{{ route("shop.productDetail", ['slug' => $product -> slug]) }}" class="btn-flat btn-hover btn-shop-now">shop now</a>
                                    <button class="btn-flat btn-hover btn-cart-add" onclick="AddCart({{$product->id}})">
                                        <i class='bx bxs-cart-add'></i>
                                    </button>
                                    <button class="btn-flat btn-hover btn-cart-add wishlist-btn" title="Add to Wish List" data-url="{{ route("wishList.addWishList", ["id" => $product->id]) }}">
                                        <i class='bx bxs-heart'></i>
                                    </button>
                                </div>
                                <div class="product-card-name">
                                    <a href="{{ route("shop.productDetail", ['slug' => $product -> slug]) }}">
                                        {{ $product -> name }}
                                    </a>
                                </div>
                                <div class="product-card-price">
                                    @if ($product->price_final == '' && $product->price_final == null)
                                    <span><del>${{ number_format($product -> price_base) }}</del></span>
                                    @else
                                    <span><del>${{ number_format($product -> price_base) }}</del></span>
                                    <span class="curr-price">${{ number_format($product -> price_final) }}</span>
                                    @endif

                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="section-footer">
                <a href="./products.html" class="btn-flat btn-hover">view all</a>
            </div>
        </div>
    <!-- end product list -->


@stop

<style>
    .top-down{
        margin: 10px 0;
        padding: 20px 0;
    }
</style>
