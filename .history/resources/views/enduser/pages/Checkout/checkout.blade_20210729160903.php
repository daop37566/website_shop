@extends("enduser.layout")
@section("front_content")
    <!-- breadcrumb-area start -->
    <div class="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    @include("enduser.components.breadcrumb", ["currentPage" => "Thanh Toán"])
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumb-area end -->

    <!-- main-content-wrap start -->
    <div class="main-content-wrap section-ptb checkout-page">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="coupon-area">
                        <!-- coupon-accordion start -->
                        <div class="coupon-accordion">
                            <h3>Have a coupon? <span class="coupon" id="showcoupon">Click here to enter your code</span></h3>
                            <div class="coupon-content" id="checkout-coupon">
                                <div class="coupon-info">
                                    <form action="#">
                                        <p class="checkout-coupon">
                                            <input type="text" placeholder="Coupon code">
                                            <button type="submit" class="btn button-apply-coupon" name="apply_coupon" value="Apply coupon">Apply coupon</button>
                                        </p>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- coupon-accordion end -->
                    </div>
                </div>
            </div>
            <!-- checkout-details-wrapper start -->
            <div class="checkout-details-wrapper">
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <!-- billing-details-wrap start -->
                        <div class="billing-details-wrap">
                            <form id="form-address-checkout">
                                <h3 class="shoping-checkboxt-title">Thông tin giao hàng</h3>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <p class="single-form-row">
                                            <label>Họ và tên <span class="required">*</span></label>
                                            <input type="text" name="name" id="ck-name" class="form-checkout">
                                            <span class="chkvl"></span>
                                        </p>
                                    </div>
                                    <div class="col-lg-12">
                                        <p class="single-form-row">
                                            <label>Số điện thoại <span class="required">*</span></label>
                                            <input type="text" name="phone" id="ck-phone" class="form-checkout">
                                            <span class="chkvl"></span>
                                        </p>
                                    </div>
                                    <div class="col-lg-12">
                                        <p class="single-form-row">
                                            <label>Email <span class="required">*</span></label>
                                            <input type="email" name="email" id="ck-email" class="form-checkout">
                                            <span class="chkvl"></span>
                                        </p>
                                    </div>
                                    <div class="col-lg-12">
                                        <p class="single-form-row">
                                            <label>Địa chỉ nhà <span class="required">*</span></label>
                                            <input type="text" name="home_address" id="ck-home-address" class="form-checkout">
                                            <span class="chkvl"></span>
                                        </p>
                                    </div>
                                    <div class="col-lg-12 mb-20">
                                        <div class="single-form-row">
                                            <select class="form-control form-checkout" id="select-province" name="province">
                                                <option>Tỉnh/ Thành Phố</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 mb-20">
                                        <div class="single-form-row">
                                            <select class="form-control form-checkout" id="select-district" name="district">
                                                <option value="districts">Quận/ Huyện</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 mb-20">
                                        <div class="single-form-row">
                                            <select class="form-control form-checkout" id="select-ward" name="ward">
                                                <option>Phường/ Xã</option>

                                            </select>
                                        </div>
                                    </div>


                                    <div class="col-lg-12">
                                        <h3 class="shoping-checkboxt-title">Phương thức thanh toán</h3>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group pure-checkbox">
                                                    <input type="radio" id="pay-cod" name="pay_method" value="cod" checked>
                                                    <label for="pay-cod">Thanh toán trực tiếp khi giao hàng</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 group_bank">
                                                <div class="form-group pure-checkbox">
                                                    <input type="radio" id="pay-atm" name="pay_method" value="atm">
                                                    <label for="pay-atm">Thanh toán qua ATM</label>
                                                    <div class="bank-options">
                                                        @foreach($banks as $key => $bank)
                                                            <div class="form-group pure-checkbox">
                                                                <input type="radio" id="atm-banking-{{ $key + 1 }}" name="bank" value="{{ $key + 1 }}">
                                                                <label for="atm-banking-{{ $key + 1 }}" class="bank-detail">
                                                                    <img src="{{ \App\Helper\Functions::getImage("bank", $bank -> picture, "thumbnail") }}" alt="{{ $bank->name }}">
                                                                    {{ $bank->name }}
                                                                </label>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-12">
                                                <div class="form-group pure-checkbox">
                                                    <input type="radio" id="pay-momo" name="pay_method" value="momo">
                                                    <label for="pay-momo">Thanh toán qua ví MoMo</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <p class="single-form-row m-0">
                                            <label>Order notes</label>
                                            <textarea name="note" placeholder="Notes about your order, e.g. special notes for delivery." class="checkout-mess" rows="2" cols="5"></textarea>
                                        </p>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- billing-details-wrap end -->
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <!-- your-order-wrapper start -->
                        <div class="your-order-wrapper">
                            <h3 class="shoping-checkboxt-title">Đơn hàng của bạn</h3>
                            <!-- your-order-wrap start-->
                            <div class="your-order-wrap">
                                <!-- your-order-table start -->
                                <div class="your-order-table table-responsive">
                                    <table>
                                        <thead>
                                        <tr>
                                            <th class="product-name th-title">Sản phẩm</th>
                                            <th class="product-total th-title">Giá</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                            <tr class="cart_item">
                                                <td class="product-name">
                                                    <strong class="product-quantity"> </strong>
                                                </td>
                                                <td class="product-total">
                                                    <span class="amount">$</span>
                                                </td>

                                        </tbody>
                                        <tfoot>
                                        <tr class="cart-subtotal">
                                            <th class="th-title">Giá tạm tính</th>
                                            <td><span class="amount">$</span></td>
                                        </tr>
                                        <tr class="cart-subtotal">
                                            <th class="th-title">Giảm giá</th>
                                            <td><span class="amount">- $0</span></td>
                                        </tr>
                                        <tr class="order-total">
                                            <th class="th-title">Tổng giá</th>
                                            <td><strong><span class="amount">$</span></strong>
                                            </td>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <!-- your-order-table end -->

                                <!-- your-order-wrap end -->
                                <div class="payment-method">
                                    <button class="btn btn-place-order" data-href="{{ route("checkout.confirmCheckout") }}">
                                        Đặt hàng
                                    </button>
                                </div>
                                <!-- your-order-wrapper start -->

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- checkout-details-wrapper end -->
        </div>
    </div>
    <!-- main-content-wrap end -->
@stop

<style>
    .text-noti{
        padding: 8px 15px;
        background: #F6F6F6;
        font-size: 24px;
        color: #3A3A3A;
        font-weight: 500;
        text-transform: capitalize;
    }
    .th-title{
        font-weight: bold !important;
    }

    .btn-place-order{
        background: #C89979 !important;
        color: white !important;
        border: none !important;
        width: 100%;
        transition: color .3s ease-in-out;
    }

    .btn-place-order:hover{
        background: #000000 !important;
    }
    .bank-options{
        visibility: hidden;
        opacity: 0;
        transition: 0.3s ease-in;
        height: 0;
    }
    .group_bank [type="radio"]:checked ~ .bank-options{
        visibility: visible;
        opacity: 1;
        height: auto;
    }

.bank-options .bank-detail{
    display: flex !important;
    align-items: center;
}

.bank-options .bank-detail{
    color: #cccccc;
    font-size: 14px;
}

.bank-options .bank-detail img{
    width: 150px;
    display: block;
    margin: 5px;
}

</style>
