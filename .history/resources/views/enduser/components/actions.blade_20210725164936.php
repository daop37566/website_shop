<div class="action-links">
    <a href="#" class="cart-btn" title="Add to Cart" data-url="{{ route("cart.addToCart", ["id" => $id_cart]) }}">
        <i class="icon-basket-loaded"></i>
    </a>
    <a  class="wishlist-btn" href="javascript:" onclick="AddCart({{$id_cart}})">
        <i class="icon-heart"></i>
    </a>
</div>
