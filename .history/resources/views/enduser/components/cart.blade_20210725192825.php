@if (Session::has('cart') != null)
<li class="cart-item">
    @foreach (Session::get('cart')->products as $item)
    <div class="cart-image d-flex justify-content-center">
        <a href="">
            <img width="50" alt="" src="">
        </a>
    </div>
    <div class="cart-title">
        <a href="">
            <h4></h4>
        </a>
        <div class="quanti-price-wrap">
            <span class="quantity ">×</span>
            <div class="price-box"><span class="new-price"></span></div>
        </div>
    </div>
    @endforeach
</li>

<li class="subtotal-box">
<div class="subtotal-title">
    <h3>Sub-Total :{{Session::get('cart')->totalPrice}}</h3>
    <span>$</span>
    <input type="number" id="total-cart"  value="{{(Session::has('cart') != null)  ? Session::get('cart')->totalQuanty : '0'}}"></input>
</div>
</li>
<li class="mini-cart-btns">
<div class="cart-btns">
    <a href="{{ route("cart.showCart") }}">Giỏ hàng</a>
    <a href="{{ route("checkout.checkLoginToCheckOut") }}">Thanh toán</a>
</div>
</li>

<li class="cart-item p-0 justify-content-center">
<span class="text-dark"> Giỏ hàng trống !!</span>
</li>
