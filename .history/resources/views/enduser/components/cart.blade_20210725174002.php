<li class="cart-item">
    <div class="cart-image d-flex justify-content-center">
        <a href="">
            <img width="50" alt="" src="">
        </a>
    </div>
    <div class="cart-title">
        <a href="">
            <h4>{{ $item['name'] }}</h4>
        </a>
        <div class="quanti-price-wrap">
            <span class="quantity ×</span>
            <div class="price-box"><span class="new-price"></span></div>
        </div>
    </div>
</li>

<li class="subtotal-box">
<div class="subtotal-title">
    <h3>Sub-Total :</h3>
    <span>$</span>
</div>
</li>
