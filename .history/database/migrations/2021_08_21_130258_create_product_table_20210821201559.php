<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->id();
            $table->string("name", 255)->nullable();
            $table->text("description")->nullable();
            $table->string("picture", 255)->nullable();
            $table->string("price_base", 20)->nullable();
            $table->string("price_final", 20)->nullable();
            $table->text("information");
            $table->string("gallery", 255)->nullable();
            $table->string("slug", 255)->nullable();
            $table->string("status", 255)->nullable();
            $table->integer("category_id", 11)->unsigned();
            $table->string("type", 255)->nullable();
            $table->string("meta_title", 255)->nullable();
            $table->text("meta_description")->nullable();
            $table->string("meta_keyword", 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
    }
}
