<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banners', function (Blueprint $table) {
            $table->id();
            $table->string("name", 255)->nullable();
            $table->string("sale", 255)->nullable();
            $table->string("picture", 255)->nullable();
            $table->text("description")->nullable();
            $table->string("price_base", 255)->nullable();
            $table->string("slug", 255);
            $table->string("location", 255)->nullable();
            $table->integer("type")->default(0);
            $table->string("status", 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banners');
    }
}
