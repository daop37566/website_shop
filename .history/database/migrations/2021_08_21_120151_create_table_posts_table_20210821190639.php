<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTablePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banners', function (Blueprint $table) {
            $table->id();
            $table->string("name", 255)->nullable();
            $table->string("picture", 255)->nullable();
            $table->string("sale", 255)->nullable();
            $table->string("description", 255)->nullable();
            $table->string("price_base", 255)->nullable();
            $table->bigInteger("type", 255)->nullable();
            $table->string("status", 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banners');
    }
}
