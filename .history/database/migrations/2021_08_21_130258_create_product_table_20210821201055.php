<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->id();
            $table->string("name", 255)->nullable();
            $table->string("discription", 255)->nullable();
            $table->string("picture", 255)->nullable();
            $table->integer("price_base")->nullable();
            $table->integer("price_final")->nullable();
            $table->text("information");
            $table->string("gallery", 255)->nullable();
            $table->string("slug", 255)->nullable();
            $table->string("status", 255)->nullable();
            $table->integer("category_id", 11)->nullable();
            $table->string("type", 255)->nullable();
            $table->string("meta_title", 255)->nullable();
            $table->text("meta_description")->nullable();
            $table->string("meta_keyword", 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
    }
}
