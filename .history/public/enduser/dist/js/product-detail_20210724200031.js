$(document).ready(function () {

    qualityControl.init();

});

document.querySelectorAll('.product-img-item').forEach(e => {
    e.addEventListener('click', () => {
        let img = e.querySelector('img').getAttribute('src')
        document.querySelector('#product-img > img').setAttribute('src', img)
    })
})

document.querySelector('#view-all-description').addEventListener('click', () => {
    let content = document.querySelector('.product-detail-description-content')
    content.classList.toggle('active')
    document.querySelector('#view-all-description').innerHTML = content.classList.contains('active') ? 'view less' : 'view all'
})

const qualityControl = {
    init: function () {
        this.setupQuanlity(
            ".js-qty-increase-2",
            ".js-qty-decrease-2",
            ".js-product-qty-2"
        );
    },
    setupQuanlity: function (increase, decrease, quality) {
        var minVal = 1,
            maxVal = 20;
        $(increase).on("click", function () {
            var parentElm = $(this).parents(".option-wrap");
            var value = parentElm.find(quality).text();

            if (value < maxVal) {
                value++;
            }
            parentElm.find(quality).text(value);
        });
        // Decrease product quantity on cart page
        $(decrease).on("click", function () {
            var parentElm = $(this).parents(".option-wrap");

            var value = parentElm.find(quality).text();
            if (value > minVal) {
                value--;
            }
            parentElm.find(quality).text(value);
        });
    },
};


