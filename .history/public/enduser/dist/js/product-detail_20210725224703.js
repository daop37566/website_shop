$(document).ready(function () {

    qualityControl.init();

});


const qualityControl = {
    init: function () {
        this.setupQuanlity(
            ".js-qty-increase-2",
            ".js-qty-decrease-2",
            ".js-product-qty-2"
        );
    },
    setupQuanlity: function (increase, decrease, quality) {
        var minVal = 1,
            maxVal = 20;
        $(increase).on("click", function () {
            var parentElm = $(this).parents(".option-wrap");
            var value = parentElm.find(quality).text();

            if (value < maxVal) {
                value++;
            }
            parentElm.find(quality).text(value);
        });
        // Decrease product quantity on cart page
        $(decrease).on("click", function () {
            var parentElm = $(this).parents(".option-wrap");

            var value = parentElm.find(quality).text();
            if (value > minVal) {
                value--;
            }
            parentElm.find(quality).text(value);
        });
    },
};

function AddCart(id){
    $.ajax({
        url : '/gio-hang/them-san-pham/'+id,
        type : 'GET',
    }).done(function(response){
        RenderCart(response);
        alertify.success('Đã Thêm Mới Sản Phẩm');
    });
}

function DeleteListCart(id) {

    $option = confirm("Bạn có muốn xóa sản phẩm này không ?");
    if(!$option){

        return;
    }
    $.ajax({
        type : 'GET',
        url : '/gio-hang/xoa-san-pham/'+id,

    }).done(function(response){
        RenderCart(response);
        alertify.success('Đã Xoa Sản Phẩm');
    });

}
$(".edit-cart").on('click',function(){
    var lists = [];
    $("table tbody tr td").each(function(){
        $(this).find("input[type=number]").each(function(){
            var ele = {key : $(this).data("id") , value : $(this).val()};
            lists.push(ele);
        });
    });
    $.ajax({
        url : 'gio-hang/cap-nhat',
        type : 'POST',
        data:{
            "_token" : "{{ csrf_token() }}",
            "data" : lists
        }
    }).done(function(response){
        location.reload();
    });
});
function RenderCart(response){
    $("#cart-list").empty();
    $("#cart-list").html(response);
    $("#total-show").text($("#total-cart").val());
}
