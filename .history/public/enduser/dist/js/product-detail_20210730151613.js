$(document).ready(function () {

    qualityControl.init();

});


const qualityControl = {
    init: function () {
        this.setupQuanlity(
            ".js-qty-increase-2",
            ".js-qty-decrease-2",
            ".js-product-qty-2"
        );
    },
    setupQuanlity: function (increase, decrease, quality) {
        var minVal = 1,
            maxVal = 20;
        $(increase).on("click", function () {
            var parentElm = $(this).parents(".option-wrap");
            var value = parentElm.find(quality).text();

            if (value < maxVal) {
                value++;
            }
            parentElm.find(quality).text(value);
        });
        // Decrease product quantity on cart page
        $(decrease).on("click", function () {
            var parentElm = $(this).parents(".option-wrap");

            var value = parentElm.find(quality).text();
            if (value > minVal) {
                value--;
            }
            parentElm.find(quality).text(value);
        });
    },
};

function AddCart(id){
    $.ajax({
        url : '/gio-hang/them-san-pham/'+id,
        type : 'GET',
    }).done(function(response){
        RenderCart(response);
        alertify.success('Đã Thêm Mới Sản Phẩm');
    });
}

function DeleteListCart(id) {
    console.log(id);
    $option = confirm("Bạn có muốn xóa sản phẩm này không ?");
    if(!$option){

        return;
    }
    $.ajax({
        type : 'GET',
        url : '/gio-hang/xoa-san-pham/'+id,

    }).done(function(response){

        RenderCart(response);
        alertify.success('Đã Xoa Sản Phẩm');
        location.reload();
    });

}
$(".edit-cart").on('click',function(){
    var lists = [];
    $("table tbody tr td").each(function(){
        $(this).find("input[type=number]").each(function(){
            var ele = {key : $(this).data("id") , value : $(this).val()};
            lists.push(ele);
        });
    });
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr("content")
        },
        url : '/gio-hang/cap-nhat',
        type : 'POST',
        data:{
            "data" : lists
        }
    }).done(function(response){
        setTimeout(()=>{
            $("#load-re").remove();
            location.reload();
        },2000);

    });
});

function RenderCart(response){
    $("#cart-list").empty();
    $("#cart-list").html(response);
    $("#total-show").text($("#total-cart").val());
}

$(".cart-quantity").on("submit", function (e){
            e.preventDefault();

            let quantity = $(this).find('input[type=number]').val();

            let url = $(this).data("url");

            $.ajax({
                type: "GET",
                url: url,
                data:{
                    "quantity": quantity
                },

                beforeSend: function (){
                    $(".overlay-snipper").addClass("open");
                },

                success: function (data){
                    if(data.code === 200){
                        $('.sweet-alert').fadeIn('slow');
                        setInterval(function (){
                            location.reload().fadeIn("slow");
                        }, 1000)
                    }
                },

                complete: function (){
                    $(".overlay-snipper").removeClass("open");
                    setTimeout(function (){
                        $('.sweet-alert').fadeOut(500);
                    },500)
                },

                error: function (){

                }
            })
        })
// $(".btn-coupon").on('click',function(){
//     let nameCoupon = $(this).parent().find(".input-text").val();
//     let url = $(this).data("url");
//     console.log(url);
//     console.log("đasa");
//     $.ajax({
//         url: url,
//         type: "GET",
//         data: {nameCoupon: nameCoupon},

//         beforeSend: function (){
//             $(".overlay-snipper").addClass("open");
//         },

//         success: function (data){
//             if(data.code === 200){
//                 // $("#cart-table").html(data.data);
//             }
//         },

//         complete: function (){
//             $(".overlay-snipper").removeClass("open");
//         },

//         error: function (){

//         }
//     })
// })

$('#select-province').change(function(){
    $.ajax({
        url : '/thanh-toan/huyen-thanh-pho/'+$(this).val(),
        type : 'GET',
    }).done(function(response){
        $("#select-district").html(response);
    });
})
$('#select-district').change(function(){
    $.ajax({
        url : '/thanh-toan/xa-phuong/'+$(this).val(),
        type : 'GET',
    }).done(function(response){
        $("#select-ward").html(response);
    });
})



