$(document).ready(function () {

    qualityControl.init();

});


const qualityControl = {
    init: function () {
        this.setupQuanlity(
            ".js-qty-increase-2",
            ".js-qty-decrease-2",
            ".js-product-qty-2"
        );
    },
    setupQuanlity: function (increase, decrease, quality) {
        var minVal = 1,
            maxVal = 20;
        $(increase).on("click", function () {
            var parentElm = $(this).parents(".option-wrap");
            var value = parentElm.find(quality).text();

            if (value < maxVal) {
                value++;
            }
            parentElm.find(quality).text(value);
        });
        // Decrease product quantity on cart page
        $(decrease).on("click", function () {
            var parentElm = $(this).parents(".option-wrap");

            var value = parentElm.find(quality).text();
            if (value > minVal) {
                value--;
            }
            parentElm.find(quality).text(value);
        });
    },
};

function AddCart(id){
    $.ajax({
        url : '/gio-hang/them-san-pham/'+id,
        type : 'GET',
    }).done(function(response){
        RenderCart(response);
        alertify.success('Đã Thêm Mới Sản Phẩm');
    });
}
function RenderCart(response){
    $("#cart-list").empty();
    $("#cart-list").html(response);
    $("#total-show").text($("#total-cart").val());
}

function DeleteListCart(id) {
    $("body").append('<div id="load-re"> <span id="preload"></span></div>');
    $option = confirm("Bạn có muốn xóa sản phẩm này không ?");
    if(!$option){
        $("#load-re").remove();
        return;
    }
    setTimeout(()=>{
        $("#load-re").remove();
        $.ajax({
        url : 'delete-list-item-cart/'+id,
        type : 'GET',

    }).done(function(response){

        RenderCart(response);

        alertify.success('Đã Xoa Sản Phẩm');
    });
        },2000);

}
